import jwt from 'jsonwebtoken';

const validateToken = (req, res, next) => {
        const authorizationHeader = req.headers.authorization;
        let result;
        if (authorizationHeader) {
            const token = req.headers.authorization.split(' ')[1];
            const options = {
                expiresIn: '2d',
            };
            try {
                result = jwt.verify(token, process.env.SECRET_JWT, options);
                // pass back the decoded token to the request object
                req.decoded = result;
                next();
            } catch (err) {
                throw new Error(err);
            }
        } else {
            result = {
                error: `Token required.`,
                status: 401
            };
            res.status(401).send(result);
        }
    }

export default validateToken;
