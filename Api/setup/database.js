import mysql, { Connection } from "mysql2"
require('dotenv').config();

const connection = mysql.createConnection({
    host: process.env.HOST_DATABASE,
    user: process.env.USER_DATABASE,
    password: process.env.PASSWORD_DATABASE,
    database: process.env.DATABASE_NAME,
    port: process.env.DATABASE_PORT || 3306,
    socketPath: process.env.SOCKETPATH_DATABASE
});

connection.connect((err) => {
    if (err) {
        console.error("error connecting: " + err.stack);
        return;
    }

    console.log("connected as id " + connection.threadId);
});

export default connection
