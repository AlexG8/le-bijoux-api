import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import morgan from 'morgan'
import fileUpload from "express-fileupload"
import router from "./router"

const server = (server) => {
    // Enable CORS
    server.use(cors())
    // Enable file upload
    server.use(fileUpload({
        createParentPath: true
    }));
    // Request body parser
    server.use(bodyParser.json())
    server.use(bodyParser.urlencoded({ extended: false }))
    server.use(express.static('../uploads')); // configure express to use uploads folder

    // Request body cookie parser
    server.use(cookieParser())
    server.use(morgan('tiny'))

    // Initializing our routes
    router(server);
}

export default server;
