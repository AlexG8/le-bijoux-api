import express from "express"

import userRoutes from "../modules/User/route"
import movieRoutes from "../modules/Movies/route"
import reviewRoutes from "../modules/Reviews/route"
import likeRoutes from "../modules/Likes/route"

const Router = (server) => {
    server.use('/api/user', userRoutes);
    server.use('/api/movie', movieRoutes);
    server.use('/api/review', reviewRoutes);
    server.use('/api/like', likeRoutes);
}
export default Router
