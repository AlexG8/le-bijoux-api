import db from "../../setup/database";

const Queries = {
    incrementLike: (req, successCallback, failureCallback) => {
        console.log(req.body);
        let sqlQuery = `UPDATE review SET likes= likes+1 WHERE review.id=${req.body.id_review}`;
        db.query(sqlQuery, (err, rows) => {
            console.log(sqlQuery)
            if (err) {
                return failureCallback(err);
            } else {
                return successCallback("Like succesfully incremented");
            }
        });
    },
    decrementLike: (req, successCallback, failureCallback) => {
        console.log(req.body);
        let sqlQuery = `UPDATE review SET likes = likes-1 WHERE review.id=${req.body.id_review}`;
        db.query(sqlQuery, (err, rows) => {
            console.log(sqlQuery)
            if (err) {
                return failureCallback(err);
            } else {
                return successCallback("Like succesfully decremented");
            }
        });
    },
}

export default Queries;
