import express from "express";
import LikeController from "./controller";

const router = express.Router();

router.put('/liked', LikeController.incrementLike);
router.put('/unliked', LikeController.decrementLike);

export default router;
