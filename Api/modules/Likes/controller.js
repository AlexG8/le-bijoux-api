import MovieService from "./service";
import ReviewServices from "../Reviews/service";

const LikeController = {
    incrementLike: (req, res) => {
        console.log(req);
        ReviewServices.incrementLike(req, (result) => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result);
        });
    },
    decrementLike: (req, res) => {
        console.log(req);
        ReviewServices.decrementLike(req, (result) => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result);
        });
    },
};

export default LikeController;
