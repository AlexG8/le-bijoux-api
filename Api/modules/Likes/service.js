import Queries from "./query";

const LikeService = {
    incrementLike: (req, callback) => {
        Queries.incrementLike(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "Like incremented by 1",
                    data: response,
                });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },
    decrementLike: (req, callback) => {
        Queries.decrementLike(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "Like decremented",
                    data: response,
                });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },
};

export default LikeService;
