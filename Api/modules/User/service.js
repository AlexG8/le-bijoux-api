import Queries from "./query"

import bcrypt from 'bcrypt'
import jwt from "jsonwebtoken"

const UserServices = {
    authenticate: async (body) => {
        let {username, password, email} = body;

        if (typeof username !== "string" || typeof password !== "string") {
            return {
                status: 400,
                payload: {success: false, message: "All fields are required and must be a string type"}
            }
        }

        const user = await Queries.getByUsername(username);

        if (!user) {
            return {status: 403, payload: {success: false, message: 'Username not found'}}
        }

        const passwordMatched = await bcrypt.compare(password, user.password);

        if (passwordMatched) {
            const token = jwt.sign({id: user.id}, process.env.SECRET_JWT);

            const {password, ...userWithoutPassword} = user;

            return ({
                status: 200,
                payload: {
                    success: true,
                    message: 'User correctly authenticated',
                    data: {'token': token, user: userWithoutPassword}
                }
            })
        }

        return {status: 403, payload: {success: false, message: 'Username & password missmatch'}}
    },
    register: async (body) => {
        const {username, email, password, avatar} = body;
        if (
            typeof username !== "string" ||
            typeof email !== "string" ||
            typeof password !== "string"
        ) {
            return {
                status: 400,
                payload: {
                    success: false,
                    message: "All fields are required and must be a string type",
                },
            };
        }

        const user = await Queries.getByUsername(username);

        if (user) {
            return {
                status: 403,
                payload: {success: false, message: "Username already exist"},
            };
        }

        return bcrypt
            .genSalt()
            .then((salt) => bcrypt.hash(password, salt))
            .then((hashedPassword) => Queries.register({username, email, hashedPassword, avatar}))
            .then((user) => ({
                status: 201,
                payload: {success: true, message: "User successfully registered"},
            }))
            .catch((err) => ({
                status: 400,
                payload: {success: false, message: err},
            }));
    },
    updateAvatar: (req, callback) => {
        Queries.updateAvatar(
            req,
            (response) => {
                return callback({ success: true, message: response });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },

}

export default UserServices;
