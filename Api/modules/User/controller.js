import UserServices from "./service"
import MovieService from "../Movies/service";
import ReviewServices from "../Reviews/service";

const UserController = {
    authenticate: (req, res) => {
        UserServices.authenticate(req.body)
            .then(result => res.status(result.status).send(result.payload))
    },
    register: (req, res) => {
        UserServices.register(req.body)
            .then(result => res.status(result.status).send(result.payload))
    },
    updateAvatar: (req, res) => {
        UserServices.updateAvatar(req, (result) => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result);
        });
    },
}

export default UserController
