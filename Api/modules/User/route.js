import express from "express"
import UserController from "./controller"
const router = express.Router();

router.post('/authenticate', UserController.authenticate);
router.post('/register', UserController.register);
router.put('/editAvatar/:id', UserController.updateAvatar);

export default router
