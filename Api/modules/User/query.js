import db from "../../setup/database";

const Queries = {
  getByUsername: (username) => {
    let sqlQuery = `SELECT * FROM user WHERE username="${username}"`;

    return new Promise((resolve, reject) => {
      db.query(sqlQuery, (err, rows) => {
        if (err) reject(err)
        resolve(rows[0])
      })
    })
  },
  register: async (users) => {
    return new Promise((resolve, reject) => {
      let sqlQuery = `INSERT INTO user (id, username, email, password, avatar, role_statut) VALUES (NULL,"${users.username}", "${users.email}", "${users.hashedPassword}", "${users.avatar}", "USER")`;

      db.query(sqlQuery, (err, res) => {
        if (err) reject(err);
        resolve(res);
      });
    });
  },
  updateAvatar: (req, successCallback, failureCallback) => {
    const url = req.body.avatarUrl;
    let sqlQuery = `UPDATE user
                     SET avatar="${url}"
                     WHERE id=${req.params.id}`;
    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback(url);
      }
    });
  },

}

export default Queries
