import Queries from "./query"

const ReviewServices = {
    getReviewsMovie: (req, callback) => {
        Queries.getReviewsMovie(
            req.params.id,
            (response) => {
                return callback({
                    success: true,
                    message: "Reviews retrieve",
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    getReviewsUser: (req, callback) => {
        Queries.getReviewsUser(
            req.params.id,
            (response) => {
                return callback({
                    success: true,
                    message: "Reviews from user retrieve",
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    postReview: (req, callback) => {
        Queries.postReview(
            req,
            response => {
                return callback({ success: true, message: response, body: req.body });
            },
            error => {
                return callback({ success: false, message: error });
            }
        )
    },
    likeReview: (req, callback) => {
        Queries.likeReview(
            req,
            response => {
                return callback({ success: true, message: response, body: req.body });
            },
            error => {
                return callback({ success: false, message: error });
            }
        )
    },
    getLikes: (id, callback) => {
        Queries.getLikes(
            id,
            (response) => {
                return callback({
                    success: true,
                    message: "Reviews likes retrieve",
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    getReviewByRate: (rate, callback) => {
        Queries.getReviewByRate(
            rate,
            (response) => {
                return callback({
                    success: true,
                    message: `Review(s) with rate ${rate} retrieve`,
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    updateReview: (req, callback) => {
        Queries.updateReview(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: `Review ${req.params.id} updated`,
                    body: req.body,
                });
            },
            (error) => {
                return callback({ success: false, message: error});
            }
        );
    },
    deleteReview: (req, callback) => {
        Queries.deleteReview(
            req.params.id,
            (response) => {
                return callback({ success: true, message: response });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },
    incrementLike: (req, callback) => {
        Queries.incrementLike(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "Like incremented by 1",
                    data: response,
                });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },
    decrementLike: (req, callback) => {
        Queries.decrementLike(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "Like decremented",
                    data: response,
                });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },
    deleteLike: (req, callback) => {
        Queries.deleteLike(
            req,
            (response) => {
                return callback({ success: true, message: response });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },
}

export default ReviewServices;
