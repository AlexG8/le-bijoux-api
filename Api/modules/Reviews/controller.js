import ReviewServices from "./service";
import MovieService from "../Movies/service";

const ReviewController = {
    getReviewsMovie: (req, res) => {
        ReviewServices.getReviewsMovie(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    getReviewsUser: (req, res) => {
        ReviewServices.getReviewsUser(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    getReviewByRate: (req, res) => {
        ReviewServices.getReviewByRate(req.params.rate, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    postReview: (req, res) => {
        ReviewServices.postReview(req, result => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result)
        })
    },
    deleteReview: (req, res) => {
        ReviewServices.deleteReview(req, (result) => {
            result.success
                ? res.status(204).send(result)
                : res.status(404).send(result);
        });
    },
    likeReview: (req, res) => {
        ReviewServices.likeReview(req, result => {
            result.success
                ? res.status(204).send(result)
                : res.status(404).send(result)
        })
    },
    updateReview: (req, res) => {
        ReviewServices.updateReview(req, (result) => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result);
        });
    },
    getLikes: (req, res) => {
        ReviewServices.getLikes(req.params.id, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    incrementLike: (req, res) => {
        ReviewServices.incrementLike(req, (result) => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result);
        });
    },
    decrementLike: (req, res) => {
        ReviewServices.decrementLike(req, (result) => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result);
        });
    },
    deleteLike: (req, res) => {
        ReviewServices.deleteLike(req, (result) => {
            result.success
                ? res.status(204).send(result)
                : res.status(404).send(result);
        });
    },
};

export default ReviewController;
