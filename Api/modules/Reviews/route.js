import express from "express"
import ReviewController from "./controller"
import validateToken  from '../../middleware/validateToken';
const router = express.Router();

// REVIEW
router.get('/movie/:id', ReviewController.getReviewsMovie);
router.get('/user/:id', validateToken, ReviewController.getReviewsUser);
router.post('/', ReviewController.postReview);
router.put('/:id', ReviewController.updateReview);
router.delete('/:id',validateToken, ReviewController.deleteReview);
router.get('/:rate', ReviewController.getReviewByRate);

// LIKE
router.get('/like/:id', ReviewController.getLikes);
router.post('/like', ReviewController.likeReview);
router.put('/like', ReviewController.incrementLike);
router.put('/liked', ReviewController.decrementLike);

// DELETE
router.post('/like/delete', ReviewController.deleteLike);

export default router
