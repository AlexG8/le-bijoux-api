import db from "../../setup/database";

const Queries = {
    getReviewsMovie: (id, successCallback, failureCallback) => {
        let sqlQuery = `SELECT review.id, review.rating, review.title, review.comment, review.likes, review.created_at, review.user_id, user.username, user.avatar
         FROM review, user WHERE movie_id = ${id} AND review.user_id = user.id`;

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback([]);
            }
        });
    },
    getReviewsUser: (id, successCallback, failureCallback) => {
        let sqlQuery = `SELECT review.id, review.rating, review.comment, review.created_at, review.user_id, movies.poster, movies.fr_title
         FROM review, movies, user WHERE review.user_id = ${id} AND review.user_id = user.id AND review.movie_id = movies.id`;

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback([]);
            }
        });
    },
    postReview: (req, successCallback, failureCallback) => {
        let sqlQuery = `INSERT INTO review (id, rating, title, comment, likes, user_id, movie_id) 
    VALUES (null, ${req.body.rating}, "${req.body.title}", "${req.body.comment}", 0,
    ${req.body.user_id}, ${req.body.movie_id})`;

        db.query(sqlQuery, (err) => {
            if (err) {
                return failureCallback(err);
            } else {
                return successCallback(`Review succesfully sent.`)
            }
        });
    },
    getReviewByRate: (rate, successCallback, failureCallback) => {
        let sqlQuery = `SELECT review.id, review.rating, review.title, review.comment, review.likes, review.created_at, review.user_id, user.username, user.avatar
         FROM review, user WHERE review.rating=${rate} AND review.user_id = user.id`

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback([]);
            }
        });
    },
    updateReview: (req, successCallback, failureCallback) => {
        let sqlQuery = `UPDATE review SET rating = '${req.body.rating}', title = '${req.body.title}', comment = '${req.body.comment}' 
        WHERE review.id = ${req.body.review_id} AND review.user_id = ${req.body.user_id}`;
        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            } else {
                return successCallback(`Review ${req.body.user_id} Updated`);
            }
        });
    },
    deleteReview: (id, successCallback, failureCallback) => {
        let sqlQuery = `DELETE FROM review WHERE id=${id}`;

        db.query(sqlQuery, (err) => {
            if (err) {
                return failureCallback(err);
            } else {
                return successCallback(`Review ${id} deleted`);
            }
        });
    },
    getLikes: (id, successCallback, failureCallback) => {
        let sqlQuery = `SELECT * FROM review_has_likes WHERE id_review = ${id}`;

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback([]);
            }
        });
    },
    likeReview: (req, successCallback, failureCallback) => {
        let sqlQuery = `INSERT INTO review_has_likes (id, id_user, id_review, id_movie) VALUES 
        (null, ${req.body.id_user}, ${req.body.id_review}, ${req.body.id_movie})`;

        db.query(sqlQuery, (err) => {
            if (err) {
                return failureCallback(err);
            } else {
                return successCallback(`Review succesfully sent.`)
            }
        });
    },
    incrementLike: (req, successCallback, failureCallback) => {
        console.log(req.body);
        let sqlQuery = `UPDATE review SET likes= likes+1 WHERE review.id=21`;
        db.query(sqlQuery, (err, rows) => {
            console.log(sqlQuery)
            if (err) {
                return failureCallback(err);
            } else {
                return successCallback("Like succesfully incremented");
            }
        });
    },
    decrementLike: (req, successCallback, failureCallback) => {
        console.log(req.body);
        let sqlQuery = `UPDATE review SET likes = likes-1 WHERE review.id=21`;
        db.query(sqlQuery, (err, rows) => {
            console.log(sqlQuery)
            if (err) {
                return failureCallback(err);
            } else {
                return successCallback("Like succesfully decremented");
            }
        });
    },
    deleteLike: (req, successCallback, failureCallback) => {
        let sqlQuery = `DELETE FROM review_has_likes WHERE id_review=${req.body.id_review} AND id_user = ${req.body.id_user}`;

        db.query(sqlQuery, (err) => {
            if (err) {
                return failureCallback(err);
            } else {
                return successCallback("like supprimer");
            }
        });
    },
}

export default Queries
