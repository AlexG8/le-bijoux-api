import MovieService from "./service";
import ReviewServices from "../Reviews/service";

const MovieController = {
    getMovies: (req, res) => {
        MovieService.getMovies(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    getMovie: (req, res) => {
        MovieService.getMovie(req.params.id, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    getMovieActors: (req, res) => {
        MovieService.getMovieActors(req.params.id, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    getMovieGenres: (req, res) => {
        MovieService.getMovieGenres(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    getMovieScreenings: (req, res) => {
        MovieService.getMovieScreenings(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    getScreening: (req, res) => {
        MovieService.getScreening(req.params.id, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },

    getSeat: (req, res) => {
        MovieService.getSeat(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    filterByFormatAndScreeningDate: (req, res) => {
        MovieService.filterByFormatAndScreeningDate(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    bookScreening: (req, res) => {
        MovieService.bookScreening(req, result => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result)
        })
    },
    deleteBooking: (req, res) => {
        MovieService.deleteBooking(req, (result) => {
            result.success
                ? res.status(204).send(result)
                : res.status(404).send(result);
        });
    },
    desactivateBooking: (req, res) => {
        MovieService.desactivateBooking(req, result => {
            result.success
                ? res.status(204).send(result)
                : res.status(404).send(result)
        })
    },
    seatOccupied: (req, res) => {
        MovieService.seatOccupied(req, result => {
            result.success
                ? res.status(204).send(result)
                : res.status(404).send(result)
        })
    },
    seatAvailable: (req, res) => {
        MovieService.seatAvailable(req, result => {
            result.success
                ? res.status(204).send(result)
                : res.status(404).send(result)
        })
    },
    nbSpectator: (req, res) => {
        MovieService.nbSpectator(req, result => {
            result.success
                ? res.status(204).send(result)
                : res.status(404).send(result)
        })
    },
    seatSelected: (req, res) => {
        MovieService.seatSelected(req, result => {
            result.success
                ? res.status(204).send(result)
                : res.status(404).send(result)
        })
    },
    nbOfScreeningSeats: (req, res) => {
        MovieService.nbOfScreeningSeats(req, result => {
            result.success
                ? res.status(204).send(result)
                : res.status(404).send(result)
        })
    },
    increaseNbOfScreeningSeats: (req, res) => {
        MovieService.increaseNbOfScreeningSeats(req, result => {
            result.success
                ? res.status(204).send(result)
                : res.status(404).send(result)
        })
    },
    getBooking: (req, res) => {
        MovieService.getBooking(req.params.id, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
    getReviews: (req, res) => {
        MovieService.getReviews(req, result => {
            result.success ? res.status(200).send(result) : res.status(404).send(result)
        })
    },
};

export default MovieController;
