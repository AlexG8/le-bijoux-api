import db from "../../setup/database";

const Queries = {
  getMovies: (param, successCallback, failureCallback) => {
    let sqlQuery = `SELECT movies.id, movies.fr_title, movies.year, movies.duration, movies.poster, movies.trailer, movies.genre_id, 
    movies.start_date, genre.id_genre, genre.label FROM movies, genre WHERE movies.genre_id = genre.id_genre`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback([]);
      }
    });
  },
  getMoviesByScreeningDate: (req, successCallback, failureCallback) => {
    let sqlQuery = `SELECT movies.id, movies.fr_title, movies.year, movies.duration, movies.poster, movies.trailer, movies.genre_id, 
    movies.start_date, genre.id_genre, genre.label FROM movies, screening, genre 
    WHERE movies.genre_id = genre.id_genre AND screening.movie_id = movies.id AND screening.screening_date = '${req.body.screening_date}'`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback([]);
      }
    });
  },
  filterByFormatAndScreeningDate: (req, successCallback, failureCallback) => {
    let sqlQuery;
    if (req.body.format && !req.body.screening_date) {
      sqlQuery = `SELECT movies.id, movies.fr_title, movies.year, movies.duration, movies.poster, movies.trailer, movies.genre_id,
    movies.start_date, genre.id_genre, genre.label FROM movies, screening, genre
    WHERE movies.genre_id = genre.id_genre AND screening.movie_id = movies.id AND screening.format = '${req.body.format}' AND DATE(NOW()) BETWEEN movies.start_date AND movies.end_date OR movies.start_date > DATE(NOW())`;
    } else if (req.body.screening_date && !req.body.format) {
      sqlQuery = `SELECT movies.id, movies.fr_title, movies.year, movies.duration, movies.poster, movies.trailer, movies.genre_id,
    movies.start_date, genre.id_genre, genre.label FROM movies, screening, genre
    WHERE movies.genre_id = genre.id_genre AND screening.movie_id = movies.id AND screening.screening_date = '${req.body.screening_date}' AND DATE(NOW()) BETWEEN movies.start_date AND movies.end_date`;
    } else {
      sqlQuery = `SELECT movies.id, movies.fr_title, movies.year, movies.duration, movies.poster, movies.trailer, movies.genre_id,
    movies.start_date, genre.id_genre, genre.label FROM movies, screening, genre
    WHERE movies.genre_id = genre.id_genre AND screening.movie_id = movies.id AND screening.screening_date = '${req.body.screening_date} AND DATE(NOW()) BETWEEN movies.start_date AND movies.end_date' 
    AND screening.format = '${req.body.format}'`;
    }

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback([]);
      }
    });
  },
  getMovie: (id, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM movies WHERE movies.id=${id}`

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No movie with this id");
      }
    });
  },
  getMovieActors: (id, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM movie_has_actor, actor, role WHERE actor.id = movie_has_actor.actor_id 
    AND movie_has_actor.role_id = role.id 
    AND movie_has_actor.movie_id=${id}`

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback([]);
      }
    });
  },
  getMovieGenres: (req, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM user`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No data found in table genre.");
      }
    });
  },
  getMovieScreenings: (req, successCallback, failureCallback) => {
    let sqlQuery = `SELECT screening.id, screening.screening_date, screening.screening_start, screening.format, screening.nb_spectator, movies.poster, movies.fr_title 
                    FROM screening, movies WHERE movies.id = ${req.params.id} AND screening.movie_id = movies.id AND DATE(screening_date) >= DATE(NOW())`

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback([]);
      }
    });
  },
  getScreening: (id, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM screening_has_seats, seat, screening WHERE screening_has_seats.seat_id = seat.id 
                    AND screening_has_seats.screening_id = screening.id AND screening.id = ${id}`

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No screening with this id");
      }
    });
  },
  getSeat: (req, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM screening_has_seats WHERE seat_id=${req.body.seat_id} AND screening_id = ${req.body.screening_id}`

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No movie with this id");
      }
    });
  },
  bookScreening: (req, successCallback, failureCallback) => {
    let sqlQuery = `INSERT INTO reservation (id, active, ticketIdentification, screening_id, user_id, vouch_id, seat_id) 
    VALUES (null, 1, ${req.body.ticketIdentificaton}, ${req.body.screening_id}, ${req.body.user_id}, ${req.body.vouch_id}, ${req.body.seat_id})`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback(`Reservation enregistrer`)
      }
    });
  },
  deleteBooking: (id, successCallback, failureCallback) => {
    let sqlQuery = `DELETE FROM reservation WHERE id=${id}`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback(`Booking deleted`);
      }
    });
  },
  desactivateBooking: (req, successCallback, failureCallback) => {
    let sqlQuery = `UPDATE reservation SET active='${req.body.active}' WHERE reservation.id = ${req.params.id}`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback(`Booking canceled`);
      }
    });
  },
  seatSelected: (req, successCallback, failureCallback) => {
    let sqlQuery = `UPDATE screening_has_seats SET seat_statut = 'selected', user_id = '${req.body.user_id}' WHERE seat_id = '${req.body.seat_id}'
                    AND screening_id = '${req.body.screening_id}'`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("Seat is selected")
      }
    });
  },
  seatAvailable: (req, successCallback, failureCallback) => {
    let sqlQuery = `UPDATE screening_has_seats SET seat_statut = 'available', user_id = NULL WHERE seat_id = '${req.body.seat_id}'
                    AND screening_id = '${req.body.screening_id}'`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("Seat is available")
      }
    });
  },
  seatOccupied: (req, successCallback, failureCallback) => {
    let sqlQuery = `UPDATE screening_has_seats SET seat_statut = 'occupied', user_id = '${req.body.user_id}' WHERE seat_id = '${req.body.seat_id}'
                    AND screening_id = '${req.body.screening_id}'`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("Seat is occupied")
      }
    });
  },
  nbOfScreeningSeats: (req, successCallback, failureCallback) => {
    let sqlQuery = `UPDATE screening SET nb_spectator = nb_spectator - 1 WHERE id = ${req.body.screening_id}`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("Screening seats decreased by 1")
      }
    });
  },
  increaseNbOfScreeningSeats: (req, successCallback, failureCallback) => {
    let sqlQuery = `UPDATE screening SET nb_spectator = nb_spectator + 1 WHERE id = ${req.body.screening_id}`;

    db.query(sqlQuery, (err) => {
      if (err) {
        return failureCallback(err);
      } else {
        return successCallback("Screening seats increased by 1")
      }
    });
  },
  getBooking: (id, successCallback, failureCallback) => {
    let sqlQuery = `SELECT user.email, vouch.label, room.label, reservation.id, seat.row, seat.number, screening.screening_date, seat.id AS seatId, screening.id AS screeningId, screening.screening_start, movies.fr_title, movies.duration,
  reservation.ticketIdentification, reservation.active FROM reservation, user, room, vouch,seat, screening,movies WHERE reservation.user_id = ${id} 
  AND reservation.user_id = user.id AND reservation.vouch_id = vouch.id
  AND reservation.screening_id = screening.id
  AND screening.movie_id = movies.id
  AND reservation.seat_id = seat.id`

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback([]);
      }
    });
  },
}

export default Queries;
