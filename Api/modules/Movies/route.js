import express from "express";
import MovieController from "./controller";
import validateToken  from '../../middleware/validateToken';

const router = express.Router();

// MOVIE
router.get('/', MovieController.getMovies);
router.get('/:id', MovieController.getMovie);
router.get('/genre', MovieController.getMovieGenres);
router.post('/filter', MovieController.filterByFormatAndScreeningDate);

// ACTOR
router.get('/actors/:id', MovieController.getMovieActors);

// SCREENING
router.get('/screenings/:id',validateToken, MovieController.getMovieScreenings);
router.get('/screening/:id', MovieController.getScreening);
router.post('/screening',validateToken, MovieController.bookScreening);

// SEAT
router.post('/seat',validateToken, MovieController.getSeat);
router.put('/seat/occupied',validateToken, MovieController.seatOccupied);
router.put('/seat/selected',validateToken, MovieController.seatSelected);
router.put('/seat/available',validateToken, MovieController.seatAvailable);


// SPECTATOR
router.put('/screening/spectator',validateToken, MovieController.nbOfScreeningSeats);
router.put('/screening/spectator/increase',validateToken, MovieController.increaseNbOfScreeningSeats);

// booking
router.get('/booking/:id',validateToken, MovieController.getBooking);
router.delete('/booking/:id',validateToken, MovieController.deleteBooking);
router.put('/booking/:id',validateToken, MovieController.desactivateBooking);

export default router;
