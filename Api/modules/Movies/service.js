import Queries from "./query";

const MovieService = {
    getMovies: (req, callback) => {
        Queries.getMovies(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "Movies retrieve",
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    getMovie: (req, callback) => {
        Queries.getMovie(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "Movie retrieve",
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    getMovieActors: (req, callback) => {
        Queries.getMovieActors(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "Actors from movie retrieve",
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    getMovieGenres: (req, callback) => {
        Queries.getMovieGenres(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "Movie genres found.",
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    getMovieScreenings: (req, callback) => {
        Queries.getMovieScreenings(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "Programmation movie retrieve",
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    getScreening: (req, callback) => {
        Queries.getScreening(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "screening found",
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    getMoviesByScreeningDate: (req, callback) => {
        Queries.getMoviesByScreeningDate(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: `Screenings with date ${req.body.screening_date} found`,
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    getSeat: (req, callback) => {
        Queries.getSeat(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "Seat found",
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    filterByFormatAndScreeningDate: (req, callback) => {
        Queries.filterByFormatAndScreeningDate(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: `Movies with ${req.body.format} retrieve`,
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    bookScreening: (req, callback) => {
        Queries.bookScreening(
            req,
            response => {
                return callback({ success: true, data: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        )
    },
    deleteBooking: (req, callback) => {
        Queries.deleteBooking(
            req.params.id,
            (response) => {
                return callback({ success: true, message: response });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },
    desactivateBooking: (req, callback) => {
        Queries.desactivateBooking(
            req,
            response => {
                return callback({ success: true, message: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        )
    },
    seatOccupied: (req, callback) => {
        Queries.seatOccupied(
            req,
            response => {
                return callback({ success: true, message: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        )
    },
    seatSelected: (req, callback) => {
        Queries.seatSelected(
            req,
            response => {
                return callback({ success: true, message: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        )
    },
    seatAvailable: (req, callback) => {
        Queries.seatAvailable(
            req,
            response => {
                return callback({ success: true, message: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        )
    },
    nbOfScreeningSeats: (req, callback) => {
        Queries.nbOfScreeningSeats(
            req,
            response => {
                return callback({ success: true, message: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        )
    },
    increaseNbOfScreeningSeats: (req, callback) => {
        Queries.increaseNbOfScreeningSeats(
            req,
            response => {
                return callback({ success: true, message: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        )
    },
    getBooking: (req, callback) => {
        Queries.getBooking(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: `Booking retrieve`,
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    getReviews: (req, callback) => {
        Queries.getReviews(
            req,
            (response) => {
                return callback({
                    success: true,
                    message: "Reviews retrieve",
                    data: response,
                });
            },
            (error) => {
                return callback({success: false, message: error});
            }
        );
    },
    postReview: (req, callback) => {
        Queries.postReview(
            req,
            response => {
                return callback({ success: true, message: response, body: req.body });
            },
            error => {
                return callback({ success: false, message: error });
            }
        )
    },
};

export default MovieService;
