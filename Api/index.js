  // Imports
import express from 'express'

// App Imports
import setupServer from './setup/server.js'
import setupStartServer from "./setup/startServer.js"

// Create express server
const server = express()

// Setup server
setupServer(server)

// Start server
setupStartServer(server)